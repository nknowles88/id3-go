// Copyright 2013 Michael Yang. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.
//
// Modifications by Nathan Knowles (gitlab: @nknowles88)

package id3

import (
	"errors"
	"io"
	"os"

	"gitlab.com/nknowles88/id3-go/v1"
	"gitlab.com/nknowles88/id3-go/v2"
)

const (
	// LatestVersion is the latest id3v2.x supported
	LatestVersion = 4
)

// Tagger represents the metadata of a tag
type Tagger interface {
	Title() string
	Artist() string
	Album() string
	AlbumArtist() string
	Composer() string
	Genre() string
	Track() string
	Disc() string
	Year() string
	Comments() []string

	SetTitle(string)
	SetArtist(string)
	SetAlbum(string)
	SetAlbumArtist(string)
	SetComposer(string)
	SetGenre(string)
	SetTrack(string)
	SetDisc(string)
	SetYear(string)

	AllFrames() []v2.Framer
	Frames(string) []v2.Framer
	Frame(string) v2.Framer
	DeleteFrames(string) []v2.Framer
	AddFrames(...v2.Framer)
	Bytes() []byte
	Dirty() bool
	Padding() uint
	Size() int
	Version() string
}

// File represents the tagged file
type File struct {
	Tagger
	OriginalSize int
	fd           *os.File
}

// Open a new tagged file
func Open(name string) (*File, error) {
	fd, err := os.OpenFile(name, os.O_RDWR, 0664)
	if err != nil {
		return nil, err
	}

	file := &File{fd: fd}

	if v2Tag := v2.ParseTag(fd); v2Tag != nil {
		file.Tagger = v2Tag
		file.OriginalSize = v2Tag.Size()
	} else if v1Tag := v1.ParseTag(fd); v1Tag != nil {
		file.Tagger = v1Tag
	} else {
		// Add a new tag if none exists
		file.Tagger = v2.NewTag(LatestVersion)
	}

	return file, nil
}

// Close a file, saving any edits to the tagged file
func (f *File) Close() error {
	defer f.fd.Close()

	if !f.Dirty() {
		return nil
	}

	switch f.Tagger.(type) {
	case *v1.Tag:
		if _, err := f.fd.Seek(-v1.TagSize, io.SeekEnd); err != nil {
			return err
		}
	case *v2.Tag:
		if f.Size() > f.OriginalSize {
			start := int64(f.OriginalSize + v2.HeaderSize)
			offset := int64(f.Tagger.Size() - f.OriginalSize)

			if err := shiftBytesBack(f.fd, start, offset); err != nil {
				return err
			}
		}

		if _, err := f.fd.Seek(0, io.SeekStart); err != nil {
			return err
		}
	default:
		return errors.New("close: unknown tag version")
	}

	if _, err := f.fd.Write(f.Tagger.Bytes()); err != nil {
		return err
	}

	return nil
}
