// Copyright 2018 Nathan Knowles. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package v2_test

import (
	"bytes"
	"gitlab.com/nknowles88/id3-go/v2"
	"testing"
)

func TestV24Frame(t *testing.T) {
	textData := []byte{
		'T', 'P', 'E', '1', // Frame ID
		0, 0, 0, 16, // Length
		0, 0, // Flags
		0, // Encoding
		'N', 'a', 't', 'h', 'a', 'n', ' ', 'K', 'n', 'o', 'w', 'l', 'e', 's', 0,
	}
	frame := v2.ParseV24Frame(bytes.NewReader(textData))
	textFrame, ok := frame.(*v2.TextFrame)
	if !ok {
		t.Errorf("ParseV24Frame on text data returns wrong type")
	}

	const text = "Nathan Knowles\000"
	if ft := textFrame.Text(); ft != text {
		t.Errorf("ParseV24Frame incorrect text, expected %s not %s", text, ft)
	}

	const encoding = "ISO-8859-1"
	if e := textFrame.Encoding(); e != encoding {
		t.Errorf("ParseV24Frame incorrect encoding, expected %s not %s", encoding, e)
	}

	if b := v2.V23Bytes(frame); !bytes.Equal(textData, b) {
		t.Errorf("V24Bytes produces different byte slice, expected %v not %v", textData, b)
	}
}
