// Copyright 2013 Michael Yang. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.
//
// Modifications by Nathan Knowles (gitlab: @nknowles88)

package v2_test

import (
	"gitlab.com/nknowles88/id3-go/v2"
	"testing"
)

func TestUnsynchTextFrameSetEncoding(t *testing.T) {
	f := v2.NewUnsynchTextFrame(v2.V23CommonFrame["Comments"], "Foo", "Bar")
	size := f.Size()
	expectedDiff := 11

	err := f.SetEncoding("UTF-16")
	if err != nil {
		t.Fatal(err)
	}
	newSize := f.Size()
	if int(newSize-size) != expectedDiff {
		t.Errorf("expected size to increase to %d, but it was %d", size+1, newSize)
	}

	size = newSize
	err = f.SetEncoding("ISO-8859-1")
	if err != nil {
		t.Fatal(err)
	}
	newSize = f.Size()
	if int(newSize-size) != -expectedDiff {
		t.Errorf("expected size to decrease to %d, but it was %d", size-1, newSize)
	}
}
