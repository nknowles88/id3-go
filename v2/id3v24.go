// Copyright 2013 Michael Yang. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.
//
// Modifications by Nathan Knowles (gitlab: @nknowles88)

package v2

import (
	"bytes"
	"fmt"
	"io"
	"os"

	"gitlab.com/nknowles88/id3-go/encodedbytes"
)

var (
	// Common frame IDs
	V24CommonFrame = map[string]FrameType{
		"Title":       V24FrameTypeMap["TIT2"],
		"Artist":      V24FrameTypeMap["TPE1"],
		"AlbumArtist": V24FrameTypeMap["TPE2"],
		"Album":       V24FrameTypeMap["TALB"],
		"Composer":    V24FrameTypeMap["TCOM"],
		"Genre":       V24FrameTypeMap["TCON"],
		"Year":        V24FrameTypeMap["TDRC"],
		"Track":       V24FrameTypeMap["TRCK"],
		"Disc":        V24FrameTypeMap["TPOS"],
		"Comments":    V24FrameTypeMap["COMM"],
	}

	// V24FrameTypeMap specifies the frame IDs and constructors allowed in ID3v2.3
	V24FrameTypeMap = map[string]FrameType{
		"AENC": {id: "AENC", description: "Audio encryption", constructor: ParseDataFrame},
		"APIC": {id: "APIC", description: "Attached picture", constructor: ParseImageFrame},
		"ASPI": {id: "ASPI", description: "Audio seek point index", constructor: ParseDataFrame},
		"COMM": {id: "COMM", description: "Comments", constructor: ParseUnsynchTextFrame},
		"COMR": {id: "COMR", description: "Commercial frame", constructor: ParseDataFrame},
		"ENCR": {id: "ENCR", description: "Encryption method registration", constructor: ParseDataFrame},
		"EQU2": {id: "EQU2", description: "Equalization (2)", constructor: ParseDataFrame},
		"ETCO": {id: "ETCO", description: "Event timing codes", constructor: ParseDataFrame},
		"GEOB": {id: "GEOB", description: "General encapsulated object", constructor: ParseDataFrame},
		"GRID": {id: "GRID", description: "Group identification registration", constructor: ParseDataFrame},
		"LINK": {id: "LINK", description: "Linked information", constructor: ParseDataFrame},
		"MCDI": {id: "MCDI", description: "Music CD identifier", constructor: ParseDataFrame},
		"MLLT": {id: "MLLT", description: "MPEG location lookup table", constructor: ParseDataFrame},
		"OWNE": {id: "OWNE", description: "Ownership frame", constructor: ParseDataFrame},
		"PRIV": {id: "PRIV", description: "Private frame", constructor: ParseDataFrame},
		"PCNT": {id: "PCNT", description: "Play counter", constructor: ParseDataFrame},
		"POPM": {id: "POPM", description: "Popularimeter", constructor: ParseDataFrame},
		"POSS": {id: "POSS", description: "Position synchronisation frame", constructor: ParseDataFrame},
		"RBUF": {id: "RBUF", description: "Recommended buffer size", constructor: ParseDataFrame},
		"RVA2": {id: "RVAD", description: "Relative volume adjustment (2)", constructor: ParseDataFrame},
		"RVRB": {id: "RVRB", description: "Reverb", constructor: ParseDataFrame},
		"SEEK": {id: "SEEK", description: "Seek frame", constructor: ParseDataFrame},
		"SIGN": {id: "SIGN", description: "Signature frame", constructor: ParseDataFrame},
		"SYLT": {id: "SYLT", description: "Synchronized lyric/text", constructor: ParseDataFrame},
		"SYTC": {id: "SYTC", description: "Synchronized tempo codes", constructor: ParseDataFrame},
		"TALB": {id: "TALB", description: "Album/Movie/Show title", constructor: ParseTextFrame},
		"TBPM": {id: "TBPM", description: "BPM (beats per minute)", constructor: ParseTextFrame},
		"TCOM": {id: "TCOM", description: "Composer", constructor: ParseTextFrame},
		"TCON": {id: "TCON", description: "Content type", constructor: ParseTextFrame},
		"TCOP": {id: "TCOP", description: "Copyright message", constructor: ParseTextFrame},
		"TDEN": {id: "TDEN", description: "Encoding time", constructor: ParseTextFrame},
		"TDLY": {id: "TDLY", description: "Playlist delay", constructor: ParseTextFrame},
		"TDOR": {id: "TDOR", description: "Original release time", constructor: ParseTextFrame},
		"TDRC": {id: "TDRC", description: "Recording time", constructor: ParseTextFrame},
		"TDRL": {id: "TDRL", description: "Release time", constructor: ParseTextFrame},
		"TDTG": {id: "TDTG", description: "Tagging time", constructor: ParseTextFrame},
		"TENC": {id: "TENC", description: "Encoded by", constructor: ParseTextFrame},
		"TEXT": {id: "TEXT", description: "Lyricist/Text writer", constructor: ParseTextFrame},
		"TFLT": {id: "TFLT", description: "File type", constructor: ParseTextFrame},
		"TIPL": {id: "TIPL", description: "Involved people list", constructor: ParseTextFrame},
		"TIT1": {id: "TIT1", description: "Content group description", constructor: ParseTextFrame},
		"TIT2": {id: "TIT2", description: "Title/songname/content description", constructor: ParseTextFrame},
		"TIT3": {id: "TIT3", description: "Subtitle/Description refinement", constructor: ParseTextFrame},
		"TKEY": {id: "TKEY", description: "Initial key", constructor: ParseTextFrame},
		"TLAN": {id: "TLAN", description: "Language(s)", constructor: ParseTextFrame},
		"TLEN": {id: "TLEN", description: "Length", constructor: ParseTextFrame},
		"TMED": {id: "TMED", description: "Media type", constructor: ParseTextFrame},
		"TOAL": {id: "TOAL", description: "Original album/movie/show title", constructor: ParseTextFrame},
		"TOFN": {id: "TOFN", description: "Original filename", constructor: ParseTextFrame},
		"TOLY": {id: "TOLY", description: "Original lyricist(s)/text writer(s)", constructor: ParseTextFrame},
		"TOPE": {id: "TOPE", description: "Original artist(s)/performer(s)", constructor: ParseTextFrame},
		"TOWN": {id: "TOWN", description: "File owner/licensee", constructor: ParseTextFrame},
		"TPE1": {id: "TPE1", description: "Lead performer(s)/Soloist(s)", constructor: ParseTextFrame},
		"TPE2": {id: "TPE2", description: "Band/orchestra/accompaniment", constructor: ParseTextFrame},
		"TPE3": {id: "TPE3", description: "Conductor/performer refinement", constructor: ParseTextFrame},
		"TPE4": {id: "TPE4", description: "Interpreted, remixed, or otherwise modified by", constructor: ParseTextFrame},
		"TPOS": {id: "TPOS", description: "Part of a set", constructor: ParseTextFrame},
		"TPRO": {id: "TPRO", description: "Produced notice", constructor: ParseTextFrame},
		"TPUB": {id: "TPUB", description: "Publisher", constructor: ParseTextFrame},
		"TRCK": {id: "TRCK", description: "Track number/Position in set", constructor: ParseTextFrame},
		"TRSN": {id: "TRSN", description: "Internet radio station name", constructor: ParseTextFrame},
		"TRSO": {id: "TRSO", description: "Internet radio station owner", constructor: ParseTextFrame},
		"TSOA": {id: "TSOA", description: "Album sort order", constructor: ParseTextFrame},
		"TSOP": {id: "TSOP", description: "Performer sort order", constructor: ParseTextFrame},
		"TSOT": {id: "TSOT", description: "Title sort order", constructor: ParseTextFrame},
		"TSRC": {id: "TSRC", description: "ISRC (international standard recording code)", constructor: ParseTextFrame},
		"TSSE": {id: "TSSE", description: "Software/Hardware and settings used for encoding", constructor: ParseTextFrame},
		"TSST": {id: "TSST", description: "Set subtitle", constructor: ParseTextFrame},
		"TXXX": {id: "TXXX", description: "User defined text information frame", constructor: ParseDescTextFrame},
		"UFID": {id: "UFID", description: "Unique file identifier", constructor: ParseIdFrame},
		"USER": {id: "USER", description: "Terms of use", constructor: ParseDataFrame},
		"USLT": {id: "USLT", description: "Unsychronized lyric/text transcription", constructor: ParseUnsynchTextFrame},
		"WCOM": {id: "WCOM", description: "Commercial information", constructor: ParseDataFrame},
		"WCOP": {id: "WCOP", description: "Copyright/Legal information", constructor: ParseDataFrame},
		"WOAF": {id: "WOAF", description: "Official audio file webpage", constructor: ParseDataFrame},
		"WOAR": {id: "WOAR", description: "Official artist/performer webpage", constructor: ParseDataFrame},
		"WOAS": {id: "WOAS", description: "Official audio source webpage", constructor: ParseDataFrame},
		"WORS": {id: "WORS", description: "Official internet radio station homepage", constructor: ParseDataFrame},
		"WPAY": {id: "WPAY", description: "Payment", constructor: ParseDataFrame},
		"WPUB": {id: "WPUB", description: "Publishers official webpage", constructor: ParseDataFrame},
		"WXXX": {id: "WXXX", description: "User defined URL link frame", constructor: ParseDataFrame},
	}
)

func ParseV24Frame(reader io.Reader) Framer {
	data := make([]byte, FrameHeaderSize)
	if n, err := io.ReadFull(reader, data); n < FrameHeaderSize || err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil
	}

	id := string(bytes.Trim(data[:4], "\x00"))
	if id == "" {
		// Reached end of frames
		return nil
	}

	t, ok := V24FrameTypeMap[id]
	if !ok {
		fmt.Fprintf(os.Stderr, "Unknown ID3v2.4 frame: %s\n", id)
		return nil
	}

	size, err := encodedbytes.SynchInt(data[4:8])
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil
	}

	if size == 0 {
		fmt.Fprintf(os.Stderr, "Found %s frame with no data\n", id)
		return nil
	}

	h := FrameHead{
		FrameType:   t,
		statusFlags: data[8],
		formatFlags: data[9],
		size:        size,
	}

	frameData := make([]byte, size)
	if n, err := io.ReadFull(reader, frameData); n < int(size) || err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil
	}

	return t.constructor(h, frameData)
}

func V24Bytes(f Framer) []byte {
	headBytes := make([]byte, 0, FrameHeaderSize)

	headBytes = append(headBytes, f.Id()...)
	headBytes = append(headBytes, encodedbytes.SynchBytes(uint32(f.Size()))...)
	headBytes = append(headBytes, f.StatusFlags(), f.FormatFlags())

	return append(headBytes, f.Bytes()...)
}
